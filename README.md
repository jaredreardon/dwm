# Jared's build of dwm

![Screenshot of my desktop](https://gitlab.com/jaredreardon/dwm/raw/main/dwm_screenshot.png)*[Link](https://wallpapercave.com/wp/wp6136109.jpg) to the wallpaper used in this screenshot*

dwm is created by the good folks at [suckless.org](https://suckless.org). This is my personal build of dwm, in which I included the following patches:
+ actualfullscreen: allow windows to become fullscreen
+ alpha-monocle: make transparent windows in monocle mode not bleed through one another when stacked atop each other
+ attachbottom: new clients appear at the bottom of the stack rather than as the master
+ borderperlayout-withindicator: allows the layout indicator to change colors based on the layout
selected, using the same colors as defined for window borders for each layout
+ canfocusfloating: toggle ability to focus floating windows (useful in conjunction with the sticky patch)
+ clientnumber: thanks to reddit user [u/ahauser31](https://www.reddit.com/user/ahauser31/) for this patch, which adds a number next to the layouts indicator displaying the number of open clients per tag (useful particularly in conjunction with the monocle gaps and monoclesymbol patches!)
+ fixborders: make window borders opaque without need of full alpha patch
+ hidevacanttags: only display tags with clients open (like i3)
+ losefullscreen: clients will lose fullscreen if a new window opens or takes focus
+ monoclesymbol: replace monocle numbers with the actual monocle symbol defined in config.h permanently
+ pertag: different layouts for different tags
+ rainbowborder: make selected window borders different colors for different window layouts
+ rotatestack: moves a window through the stack, in either direction
+ scratchpad: temporary pop-up terminals
+ status2d-xrdb: allow color in the statusbar, optionally using Xresources colors
+ statuscmd: dwm signaling, allowing for things like clickable modules in the bar with a patched
dwmblocks
+ sticky: make windows sticky (stick across tag changes)
+ swallow: if a program run from a terminal would make it inoperable, it temporarily takes its place to save space
+ vanitygaps: adds gaps to all layouts (including to the monocle layout, with the help of [u/breakfict](https://www.reddit.com/user/breakfict/) on reddit!)
+ xrdb: allow dwm to use colors from Xresources file

# Dependencies
+ [st](https://st.suckless.org/)
+ [dmenu](https://tools.suckless.org/dmenu/)
+ libxft-bgra-git (AUR)

NOTE: I don't use Debian or Ubuntu, or their derivatives, so I'm not familiar with whether or not extra steps must be taken or extra packages installed to make my build work on those distros.

# Installation

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/jaredreardon/dwm.git
	cd dwm
	sudo make clean install

NOTE: Installing my dwm build will overwrite your existing dwm installation!

# My Keybindings

My build sets the MODKEY to the Super key.

| Keybinding                | Action                                                                                                                                |
| :---                      | :---                                                                                                                                  |
| `MODKEY + RETURN`         | opens terminal (st)                                                                                                                   |
| `MODKEY + SHIFT + RETURN` | make current client the master                                                                                                        |
| `MODKEY + d`              | opens run launcher (dmenu)                                                                                                            |
| `MODKEY + SHIFT + q`      | closes window with focus                                                                                                              |
| `MODKEY + 1-9`            | switch focus to workspace (1-9)                                                                                                       |
| `MODKEY + SHIFT + 1-9`    | send focused window to workspace (1-9)                                                                                                |
| `MODKEY + j`              | focus stack +1 (switches focus between windows in the stack)                                                                          |
| `MODKEY + k`              | focus stack -1 (switches focus between windows in the stack)                                                                          |
| `MODKEY + SHIFT + j`      | rotate stack +1 (rotates the windows in the stack)                                                                                    |
| `MODKEY + SHIFT + k`      | rotate stack -1 (rotates the windows in the stack)                                                                                    |
| `MODKEY + h`              | setmfact -0.05 (expands size of window)                                                                                               |
| `MODKEY + l`              | setmfact +0.05 (shrinks size of window)                                                                                               |
| `MODKEY + .`              | focusmon +1 (switches focus next monitors)                                                                                            |
| `MODKEY + ,`              | focusmon -1 (switches focus to prev monitors)                                                                                         |
| `MODKEY + space`          | cycle between tiled (tall), bottomstack (wide), and monocle (full) layouts                                                            |
| `MODKEY + f`              | make window fullscreen                                                                                                                |
| `MODKEY + backslash`      | spawn scratchpad terminal                                                                                                             |
| `MODKEY + z`              | increase gaps|
| `MODKEY + x`              | decrease gaps                                                                                                        |
| `MODKEY + a`              | restore default gaps                                                                                                        |
| `MODKEY + q`              | dmenu prompt to lock ([slock](https://tools.suckless.org/slock/)), suspend, reboot, or shutdown system, or to logout of your X server |

# Running dwm

If you're a true minimalist Gigachad who forgoes a display manager in favor of xinit, you can add the following line to the end of your .xinitrc to start dwm using startx:

    exec dwm

# Configuring dwm

The configuration of dwm is done by editing the config.h file and (re)compiling the source code. The source code will be located where you git cloned it but in case you want to copy me, I keep mine in ~/.local/src. Type the following command to recompile dwm and then logout and back into your X server.

	sudo make clean install
