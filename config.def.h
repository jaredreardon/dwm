/* See LICENSE file for copyright and license details. */

#define myTerm "st"
#define termClass "St"

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 12;       /* snap pixel */
static const unsigned int gappih    = 24;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 24;       /* vert inner gap between windows */
static const unsigned int gappoh    = 24;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 24;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10:antialias=true:autohint=true", "JoyPixels:size=8:antialias=true:autohint=true", };
static const char dmenufont[]       = "monospace:size=10:antialias=true:autohint=true";
static char *colors[][2] = {
        /*                  fg              bg          */
    	[SchemeNorm]    = { normfgcolor,    normbgcolor },
	    [SchemeSel]     = { selfgcolor,     selbgcolor  },
};

static const char *border_colors[][2] = {
	/*  unsel	            sel   */
	{   normbordercolor,    selbgcolor  },
	{   normbordercolor,    termcol1    },
	{   normbordercolor,    termcol5    },
	{   normbordercolor,    selbgcolor  },
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {myTerm, "-n", "spterm", "-g", "100x30", "-t" "scratchpad", NULL };
const char *spcmd2[] = {myTerm, "-n", "spcalc", "-g", "25x10", "-f", "monospace:size=20", "-t", "calculator", "-e", "bc", "-lq", NULL };
const char *spcmd3[] = {myTerm, "-n", "spsound", "-g", "100x30", "-e", "pulsemixer", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spcalc",	    spcmd2},
	{"spsound",	    spcmd3},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* class	    instance	title		        tags mask       isfloating	isterminal  noswallow   monitor */
	{ termClass,	NULL,		NULL,       	    0,            	0,          1,         	0,        	-1 },
    { "discord",    NULL,       NULL,       	    1 << 8,         0,          0,          0,          -1 },
    { "Signal",     NULL,       NULL,       	    1 << 8,         0,          0,          0,          -1 },
   	{ "Element",    NULL,       NULL,       	    1 << 8,         0,          0,          0,          -1 },
   	{ "Gimp",       NULL,       NULL,       	    1 << 7,         0,          0,          0,          -1 },
   	{ "Steam",       NULL,       NULL,       	    1 << 7,         0,          0,          0,          -1 },
    { NULL,		    NULL,		"Polls/Quizzes",	0,	            1,		    0,		    0,		    -1 },
    { NULL,		    NULL,		"zoom",	            0,	            1,		    0,		    0,		    -1 },
    { NULL,		    NULL,		"Event Tester",		0,	            0,		    0,		    1,		    -1 },
	{ NULL,		    NULL,		"vifm",       	    0,            	0,          0,         	1,        	-1 },
	{ NULL,		    NULL,		"ani-cli",       	0,            	0,          0,         	1,        	-1 },
	{ NULL,		    "spterm",	NULL,		        SPTAG(0),	    1,		    1,	 	    0,		    -1 },
	{ NULL,		    "spcalc",	NULL,		        SPTAG(1),	    1,		    1,	 	    0,		    -1 },
	{ NULL,		    "spsound",	NULL,		        SPTAG(2),	    1,		    1,	 	    0,		    -1 },
	{ NULL,		    "spvifm",	NULL,		        SPTAG(4),	    1,		    1,	 	    0,		    -1 },
	{ NULL,		    NULL,		"Katawa Shoujo",    0,            	1,          0,         	0,        	-1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
    /* index    symbol      arrange function */
	{ 0,        "[]=",   tile    },
	{ 1,        "TTT",   bstack  },
	{ 2,        "[M]",   monocle },
	{ 3,        NULL,       NULL    },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0";
static const char *dmenucmd[] = { "dmenu_run", "-c", "dmenu_ns", "-p", "run:", "-m", dmenumon, NULL };
static const char *termcmd[]  = { "st", NULL };

#include <X11/XF86keysym.h>
#include "movestack.c"
#define CLICKABLE_BLOCKS

static Key keys[] = {
	/* modifier                     key             function        argument */
	{ MODKEY,                       XK_d,      	    spawn,          {.v = dmenucmd } },
	{ MODKEY,             		    XK_Return, 	    spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_j,      	    focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      	    focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,           movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,           movestack,      {.i = -1 } },
	{ MODKEY,                       XK_h,      	    setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,       	setmfact,       {.f = +0.05} },
	{ MODKEY,			            XK_apostrophe,	incnmaster,     {.i = +1 } },
	{ MODKEY,		                XK_semicolon,	incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Return,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,    	    view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      	    killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_space,  	    togglefloating, {0} },
	{ MODKEY,                       XK_f,           togglefullscr,  {0} },
	{ MODKEY,                       XK_comma,  	    focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, 	    focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  	    tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, 	    tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_r,           quit,           {0} },
    { MODKEY,			            XK_space,       cyclelayout,    {.i = +1 } },
	{ MODKEY,            	        XK_backslash,	togglescratch,  {.ui = 0 } },
	{ MODKEY,            		    XK_c,	        togglescratch,  {.ui = 1 } },
	{ MODKEY,            		    XK_a,	        togglescratch,  {.ui = 2 } },

	/* Program Hotkeys */
	{ MODKEY,			            XK_e,		    spawn,		    SHCMD(myTerm " -e neomutt") },
	{ MODKEY,			            XK_g,	    	spawn,  		SHCMD("gimp") },
	{ MODKEY,			            XK_i,		    spawn,	    	SHCMD(myTerm " -e htop") },
	{ MODKEY,			            XK_m,		    spawn,	    	SHCMD(myTerm " -e ncmpcpp") },
    { MODKEY,			            XK_n,	    	spawn,	    	SHCMD(myTerm " -e newsboat") },
	{ MODKEY,			            XK_r,		    spawn,	    	SHCMD(myTerm " -e vifm") },
	{ MODKEY,			            XK_s,		    spawn,		    SHCMD("dmenusearch") },
	{ MODKEY,			            XK_t,		    spawn,	    	SHCMD(myTerm " -e cointop") },
	{ MODKEY,			            XK_w,	    	spawn,	    	SHCMD("$BROWSER") },
	{ MODKEY,			            XK_y,		    spawn,		    SHCMD(myTerm " -e calcurse") },
	{ MODKEY,			            XK_Scroll_Lock,		    spawn,		    SHCMD("farge --notify") },

	/* Audio Controls */
	{ 0, 				            XF86XK_AudioMute,           spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ 0, 				            XF86XK_AudioRaiseVolume,	spawn,		SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ 0, 				            XF86XK_AudioLowerVolume,	spawn,		SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
	{ 0, 				            XF86XK_AudioMicMute,		spawn,		SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ MODKEY,			            XK_minus,			        spawn,		SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,			            XK_equal,			        spawn,		SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,			            XK_BackSpace,			    spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		        XK_BackSpace,			    spawn,		SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0,				            XF86XK_Launch1,			    spawn,      SHCMD(myTerm " -e pulsemixer") },

	/* Brightness Controls */
	{ 0, 				            XF86XK_MonBrightnessUp,             spawn,		SHCMD("brightnessctl set 5%-; kill -45 $(pidof dwmblocks)") },
	{ 0, 				            XF86XK_MonBrightnessDown,           spawn,		SHCMD("brightnessctl set +5%; kill -45 $(pidof dwmblocks)") },

	/* Music Controls */
	{ MODKEY,			            XK_Left,		            spawn,		SHCMD("mpc prev") },
	{ MODKEY,			            XK_Right,		            spawn,		SHCMD("mpc next") },
	{ MODKEY,			            XK_Down,		            spawn,		SHCMD("mpc toggle") },
	{ MODKEY,			            XK_bracketleft,		        spawn,		SHCMD("mpc seek -5") },
	{ MODKEY,			            XK_bracketright,	        spawn,		SHCMD("mpc seek +5") },

	/* Screen Recording & Capture*/
	{ 0,				            XK_Print,	    spawn,		SHCMD("maim -u $HOME/pix/screenshots/$(date '+%Y_%m_%d_screenshot').png") },
	{ MODKEY,			            XK_Print,	    spawn,		SHCMD("maimpick") },
	{ MODKEY,			            XK_Insert,	    spawn,		SHCMD("dmenurecord") },
	{ MODKEY,			            XK_Delete,	    spawn,		SHCMD("dmenurecord kill") },

	/* dmenu Key Scripts */
	{ MODKEY,			            XK_b,		                spawn,		SHCMD("dmenubook") },
	{ MODKEY,			            XK_p,		                spawn,		SHCMD("passmenu -c 'dmenu_ns' -p 'passwords:'") },
	{ MODKEY,			            XK_q,		                spawn,		SHCMD("dmenupower") },
    { MODKEY|ShiftMask,			    XK_v,		                spawn,		SHCMD("ytfzf -D") },
    { MODKEY,			            XK_v,		                spawn,		SHCMD("ytfzf -DS --video-limit=165 --sort") },
	{ MODKEY,			            XK_grave,	                spawn,		SHCMD("dmenuunicode") },
	{ MODKEY,			            XK_F1,		                spawn,		SHCMD("dmenudoppler") },
    { MODKEY,			            XK_F3,                      spawn,		SHCMD("dmenudisplay") },
	{ MODKEY,			            XK_F9,		                spawn,		SHCMD("dmenumount") },
	{ MODKEY,			            XK_F10,		                spawn,		SHCMD("dmenuumount") },

	/* Tags */
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigstatusbar,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigstatusbar,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigstatusbar,   {.i = 6} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1 } },
};
